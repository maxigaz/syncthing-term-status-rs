# Syncthing Term Status

A simple TUI application that shows the current Syncthing status, including the list of known devices and their connection, paused, and sync status.

It is also a Rust rewrite of [my own application](https://codeberg.org/maxigaz/syncthing-term-status/), originally written in Python.

## Current Project Status

The software is currently considered pre-alpha: don't expect it to do much. For now, please, only use it for testing purposes!

## Requirements

- `ncurses`

## Credits

- [cursive](https://github.com/gyscos/cursive)

## Licence

GPLv3
