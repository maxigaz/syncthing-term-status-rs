use cursive::theme::{BorderStyle, Palette};
use cursive::traits::*;
use cursive::views::{Dialog, EditView, LinearLayout, TextView};
use cursive::Cursive;
use std::env;
use ureq::serde_json;

fn print_ids(url: String, api_key: String) -> Result<String, ureq::Error> {
    let body: serde_json::Value = ureq::get(&url)
        // TODO: Set API Key from input field
        .set("X-API-key", &api_key)
        .call()?
        .into_json()?;

    // Print all the device IDs
    let mut device_ids: Vec<String> = Vec::new();
    // The as_array() helper method is used, which returns Option, allowing us
    // to handle only valid array cases
    if let Some(devices) = body["devices"].as_array() {
        for i in devices {
            // println!("{}", i["deviceID"]);
            device_ids.push(i["deviceID"].to_string());
        }
    }

    Ok(device_ids.join(", "))
}

fn main() {
    // Creates the cursive root - required for every application.
    let mut siv = cursive::default();

    // Load theme file
    // siv.load_theme_file("assets/style.toml").unwrap();

    siv.set_theme(cursive::theme::Theme {
        shadow: true,
        borders: BorderStyle::Simple,
        palette: Palette::terminal_default().with(|palette| {
            use cursive::theme::BaseColor::*;

            {
                // First, override some colors from the base palette.
                use cursive::theme::Color::TerminalDefault;
                use cursive::theme::PaletteColor::*;

                palette[Background] = TerminalDefault;
                palette[View] = TerminalDefault;
                palette[Primary] = White.dark();
                palette[TitlePrimary] = Blue.light();
                palette[Secondary] = Black.light();
                palette[Highlight] = Blue.dark();
                palette[HighlightText] = TerminalDefault;
            }

            {
                // Then override some styles.
                // use cursive::theme::Effect::*;
                // use cursive::theme::PaletteStyle::*;
                // use cursive::theme::Style;
                // palette[Highlight] = Style::from(Blue.light()).combine(Bold);
            }
        }),
    });

    // Collect command-line arguments
    let args: Vec<String> = env::args().collect();
    // dbg!(&args);

    // When a single argument is provided, the length becomes 2.
    // Fill APIInput with the <api_key>.
    let mut arg_key = String::new();
    if args.len() == 2 {
        arg_key = args[1].to_string();
    }

    // Add a layer to the current screen
    siv.add_layer(
        Dialog::new()
            .title("Connect to Syncthing Instance")
            .padding_lrtb(1, 1, 1, 0)
            // .content(TextArea::new().with_name("Syncthing URL"))
            // .content(TextArea::new().with_name("API Key"))
            .content(
                LinearLayout::vertical()
                    .child(TextView::new("Syncthing Instance URL").with_name("URLInputLabel"))
                    .child(EditView::new().content("http://127.0.0.1:8384").with_name("URLInput"))
                    .child(TextView::new("API Key").with_name("APIInputLabel"))
                    // NOTE: Cloning arg_key is only a workaround for using it in two different places.
                    .child(EditView::new().content(arg_key.clone()).with_name("APIInput")).min_width(34)
            )
            .button("OK", move |s| {
                let api_key = s.find_name::<EditView>("APIInput").unwrap().get_content();
                s.pop_layer();
                s.add_layer(Dialog::new()
                    .title("Syncthing Status")
                    .content(
                        LinearLayout::vertical()
                            .child(TextView::new(print_ids("http://127.0.0.1:8384/rest/config".to_string(), api_key.to_string()).unwrap()))
                    )
                );
            })
            .button("Cancel", Cursive::quit),
    );

    // Starts the event loop.
    siv.run();
}
